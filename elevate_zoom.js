(function ($) {
  Drupal.behaviors.elevateZoom = {
    attach: function (context, settings) {
      $('#elevate-zoom-wrapper').toggleClass('nojs js');
      console.log(settings.elevateZoom);
      $("#elevate-zoom").elevateZoom(settings.elevateZoom);
    }
  }
})(jQuery);